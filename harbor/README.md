# Harbor
See [Deploying Harbor with High Availability via Helm](https://goharbor.io/docs/2.4.0/install-config/harbor-ha-helm/).

## Database setup
On coffee, switch to the `postgres` user, run `psql`, and execute the following:
```sql
CREATE USER harbor WITH PASSWORD 'REPLACE_ME';
CREATE DATABASE harbor_registry OWNER harbor;
REVOKE ALL ON DATABASE harbor_registry FROM PUBLIC;
CREATE DATABASE harbor_notary_server OWNER harbor;
REVOKE ALL ON DATABASE harbor_notary_server FROM PUBLIC;
CREATE DATABASE harbor_notary_signer OWNER harbor;
REVOKE ALL ON DATABASE harbor_notary_signer FROM PUBLIC;
```

## Redis setup
See [syscom-redis.yaml](../syscom-redis.yaml). The reason why this is in the
syscom namespace is because we may decide to re-use this Redis server for
other apps.

## Install the Helm chart
Open values.yaml and replace all instances of 'REPLACE_ME' with appropriate username/password values.

Check https://artifacthub.io/packages/helm/harbor/harbor to see what the latest **stable** version is.
```sh
helm install -f values.yaml --create-namespace --namespace harbor harbor1 harbor/harbor --version 1.8.1
```
